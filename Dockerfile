FROM node:17.2.0-bullseye
COPY . .
RUN npm install
EXPOSE 8000
CMD npm start
WORKDIR /home/node/app
